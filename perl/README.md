# Perl

## String Manipulation

The below code example prints the character e 50 times.

````perl
print "e"x50
````