# C

## Memory Management

Memory management in C is done via malloc()/free() traditionally; Unlike cpp
the size of the memory block is not determined by the type.

````c
int *a = malloc(sizeof(int));
if(a){
    *a = 10;
}
````

Below is an example of allocating an array.

````c
int *p = malloc(sizeof(int) * 4);
if(p){
    p[0] = 1;
    p[1] = 2;
}
````