# Gdb

Very useful tool for working with binaries.

## Commands

### Run Program With Arguments

````sh
r 'arg1' 'arg2'
````

### Examine Memory

Examine a region of memory ( of size 100 ) as hex.
The trailing x can be replaced with a b for binary. ( Other options exist )

````sh
x/100x
````

### Print Registers

````sh
print $rbp
````

### Set Breakpoints

````sh
break $rbp    # Break on value in a register
break *0x100f # Break on memory address
break _main   # Break on a label
break strcpy  # Break on a function call
````