# KnowledgeBase

A repository containing notes and example programs on things I find interesting.

## Index

1. [Perl](./perl/README.md)
2. [C](./c/README.md)
3. [Gdb](./gdb/README.md)